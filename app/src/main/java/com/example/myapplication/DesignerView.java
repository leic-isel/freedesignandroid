package com.example.myapplication;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import java.util.LinkedList;

public class DesignerView extends View {

    Designer designer;
    LinkedList<LinkedList<Point>> listOfPointsLists = new LinkedList<>();

    public DesignerView(Designer designer) {
        super(designer);
        this.designer = designer;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
        Point point = new Point(event);
        if( action == MotionEvent.ACTION_DOWN ){
            LinkedList<Point> pointsList = new LinkedList<>();
            listOfPointsLists.add(pointsList);
            pointsList.add(point);
            designer.mensage(point);
            return true;
        } else if ( action == MotionEvent.ACTION_MOVE ) {
            LinkedList<Point> pointsList = listOfPointsLists.getLast();
            pointsList.add(point);
            invalidate();
            //designer.mensage();
            return true;
        }
        else return super.onTouchEvent(event);
    }


    private Paint paint = new Paint();
    {
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        Point aux = null;
        for (LinkedList<Point> list : listOfPointsLists){
            for (Point pos : list){
                if(aux == null) aux = pos;
                else {
                    canvas.drawLine(aux.getX(), aux.getY(), pos.getX(), pos.getY(), paint);
                    aux = pos;
                }
            }
            aux = null;
        }
    }


    public void reset(){
        listOfPointsLists.clear();
        invalidate();
    }
}

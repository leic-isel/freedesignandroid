package com.example.myapplication;

import android.view.MotionEvent;

import androidx.annotation.NonNull;

public class Point {
    private int x, y;

    public Point(MotionEvent event){
        x=(int)event.getX();
        y=(int)event.getY();
    }

    public int getX(){ return x; }
    public int getY(){ return y; }

    @NonNull
    @Override
    public String toString() {
        return "First: ("+x+"; "+y+")";
    }
}

package com.example.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.LinkedList;

public class Designer extends Activity {

    private Button save, load, reset;
    private DesignerView draw;

    @Override
    protected void onCreate(Bundle State) {
        super.onCreate(State);

        save = new Button(this);
        save.setText("Save");
        load = new Button(this);
        load.setText("Load");
        reset = new Button(this);
        reset.setText("Reset");

        LinearLayout main = new LinearLayout(this);
        main.setOrientation(LinearLayout.VERTICAL);
        LinearLayout buttons = new LinearLayout(this);
        buttons.setOrientation(LinearLayout.HORIZONTAL);
        draw = new DesignerView(this);
        draw.setBackgroundColor(Color.GRAY);

        buttons.addView(save);
        buttons.addView(load);
        buttons.addView(reset);
        main.addView(buttons);
        main.addView(draw);

        setContentView(main);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draw.reset();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        int i = 0;

        for (LinkedList<Point> listPoint : draw.listOfPointsLists){
            for(Point point : listPoint){
                state.putInt("PointsX"+i, point.getX());
                state.putInt("pointsY"+i, point.getY());
                i++;
            }
        }

        //state.putInt("Counter", 12);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle state) {
        super.onRestoreInstanceState(state);


        //add(state.getInt("Counter"));
    }



    //mesagem para debug
    public void mensage(Point point) {
        Toast.makeText(this, point.toString(), Toast.LENGTH_SHORT).show();
    }
}
